import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import HomePage from './component/home/HomePage'
import Navbar from './component/common/Navbar';
import FindOwner from './component/owner/FindOwner';
import VeterinariansPage from './component/veterinarian/VeterinariansPage';
import ErrorPage from './component/error/ErrorPage';
import OwnerInformation from './component/owner/OwnerInformation';
import OwnerCreation from './component/owner/OwnerCreation';
import OwnerEdit from './component/owner/OwnerEdit';
import OwnersPage from './component/owner/OwnersPage';
import PetCreation from './component/pet/PetCreation';
import PetEdit from './component/pet/PetEdit';
import VisitCreation from './component/visit/VisitCreation';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class App extends Component {

  state = {
    loading: true
  }

  componentDidMount() {
    this.setState({loading: false})
  }

  render() {
    return (
      <div>
        <Navbar />
        <Switch>
          <Route exact path="/" component={HomePage} />

          <Route exact path="/veterinarians" component={VeterinariansPage} />

          <Route exact path="/findOwner" component={FindOwner} />
          <Route exact path="/owners" component={OwnersPage} />
          <Route exact path="/ownerInformation/:lastname" component={OwnerInformation} />
          <Route exact path="/ownerCreation" component={OwnerCreation} />
          <Route exact path="/ownerEdit/:id" component={OwnerEdit} />

          <Route exact path="/petCreation/:owner_id" component={PetCreation} />
          <Route exact path="/editPet/:id" component={PetEdit} />

          <Route exact path="/visitCreation/:pet_id" component={VisitCreation} />

          <Route exact path="/error" component={ErrorPage} />
          <Route component={ErrorPage} />
        </Switch>
        <ToastContainer />
      </div>
    )
  }
}

export default App
