import React, { Component } from 'react'
import logo from '../../assets/img/animals.png'

export default class HomePage extends Component {

  render() {
    return (
        <div className="container home">
          <h1>Jason's Petclinic</h1>
          <h2>Welcome</h2>
          <img src={logo} alt="animals" />
        </div>
    )
  }
  
}
