import React, { Component } from 'react'
import '../../assets/css/Navbar.css'
import { NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faSearch, faList, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'

export default class Navbar extends Component {

  render() {
    return (
      <nav>
        <ul>
            <li>
                <NavLink exact to="/" activeClassName="active"><FontAwesomeIcon icon={faHome} /> HOME</NavLink>
            </li>
            <li>
                <NavLink to="/findOwner" activeClassName="active"><FontAwesomeIcon icon={faSearch} /> FIND OWNERS</NavLink>
            </li>
            <li>
                <NavLink to="/veterinarians" activeClassName="active"><FontAwesomeIcon icon={faList} /> VETERINARIANS</NavLink>
            </li>
            <li>
                <NavLink to="/error" activeClassName="active"><FontAwesomeIcon icon={faExclamationTriangle} /> ERROR</NavLink>
            </li>
        </ul>
      </nav>
    )
  }

}