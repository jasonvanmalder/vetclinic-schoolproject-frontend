import React from 'react'
import loader from '../../assets/img/loader.svg'

const Loader = ({height, width}) => {

    const divStyle = {
        width: '100%',
        textAlign: 'center'
    }

    const imageStyle = {
        height: (height) ? height : 64,
        width: (width) ? width : 64
    }

    return (
        <div style={divStyle}>
            <img src={loader} style={imageStyle} alt="Loader" />
        </div>
    )

}

export default Loader