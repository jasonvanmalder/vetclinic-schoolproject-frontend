import React, { Component } from 'react'
import logo from '../../assets/img/sad-dog.png'

export default class ErrorPage extends Component {

  render() {
    const imageStyle = {
      borderRadius: '50%',
      border: '3px solid #6db33f',
      width: '400px',
      height: '400px'
    }

    return (
        <div className="container home">
            <h1>Oops!</h1>
            <img src={logo} style={imageStyle} alt="Error logo" />
            <h2>Something went wrong...</h2>
        </div>
    )
  }

}