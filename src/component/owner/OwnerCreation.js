import React, { Component } from 'react'
import '../../assets/css/OwnersPage.css'
import axios from 'axios'
import FormChecker from '../../services/FormChecker'

export default class OwnerCreation extends Component {

    createOwner = async () => {

        const Owner = {
            firstname: this.firstnameInput.value,
            lastname: this.lastnameInput.value,
            address: this.addressInput.value,
            city: this.cityInput.value,
            telephone: this.telephoneInput.value,
        }

        if(FormChecker.isString(Owner.firstname, 2)) {
            if(FormChecker.isString(Owner.lastname, 2)) {
                if(FormChecker.checkLength(Owner.address, 10)) {
                    if(FormChecker.isString(Owner.city, 4)) {
                        if(FormChecker.isPhoneNumber(Owner.telephone)) {
                            try {
                                await axios.post('http://localhost:8080/v1/createOwner', Owner);
                                this.infoParagraph.innerHTML = 'Owner added'
                            } catch (e) {
                                this.infoParagraph.innerHTML = 'Can\'t add owner'
                            }
                    
                            if (this.infoParagraph) {
                                this.infoParagraph.style.display = 'block'
                            }
                        }else {
                            FormChecker.errorMessage('Invalid phone number')
                        }
                    }else {
                        FormChecker.errorMessage('Invalid city')
                    }
                }else {
                    FormChecker.errorMessage('Invalid address')
                }
            }else {
                FormChecker.errorMessage('Invalid name')
            }
        }else {
            FormChecker.errorMessage('Invalid firstname')
        }
    }

    render() {
        return (
            <div className="container">
                <h2>Owner</h2>

                <p ref={(info) => this.infoParagraph = info} style={{display:'none'}}></p>

                <div className="form-container">
                    <div className="input-group">
                        <label>First name</label>
                        <input type="text" placeholder="First name" ref={(input) => this.firstnameInput = input} />
                    </div>

                    <div className="input-group">
                        <label>Last name</label>
                        <input type="text" placeholder="Last name" ref={(input) => this.lastnameInput = input} />
                    </div>

                    <div className="input-group">
                        <label>Address</label>
                        <input type="text" placeholder="Address" ref={(input) => this.addressInput = input} />
                    </div>

                    <div className="input-group">
                        <label>City</label>
                        <input type="text" placeholder="City" ref={(input) => this.cityInput = input} />
                    </div>

                    <div className="input-group">
                        <label>Telephone</label>
                        <input type="text" placeholder="Telephone" ref={(input) => this.telephoneInput = input} />
                    </div>

                    <button onClick={this.createOwner}>Add Owner</button>
                </div>
            </div>
        )
    }
    
}
