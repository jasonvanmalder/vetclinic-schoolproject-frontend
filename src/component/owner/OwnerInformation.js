import React, { Component } from 'react'
import axios from 'axios'
import '../../assets/css/OwnerInformation.css'
import PetInfo from '../pet/PetInfo'
import Loader from '../common/Loader'
import OwnerInformationTableEntry from './OwnerInformationTableEntry'

export default class OwnerInformation extends Component {
    
    state = {
        owner: null,
        pets: [],
        loadingOwner: true,
        loadingPets: true
    }

    getOwner = async () => {
        const { lastname } = this.props.match.params
        const response = await axios.get('http://localhost:8080/v1/getOwner/' + lastname)
        return await response
    }

    setOwner = () => {
        this.getOwner()
        .then(response => this.setState({owner: response.data, loadingOwner: false}))
        .catch((err) => this.props.history.push('/error'))
    }

    getPets = async () => {
        const response = await axios.get('http://localhost:8080/v1/getPets/' + this.state.owner.id)
        return await response
    }

    setPets = () => {
        this.getPets()
        .then((response) => {
            let pets = []

            response.data.forEach(pet => {
                pets.push(
                    <PetInfo 
                        key={pet.id}
                        {...pet}
                        history={this.props.history}
                    />
                )
            })

            this.setState({pets, loadingPets: false})
        })
        .catch((err) => {
            this.setState({loadingPets: false})
            if (this.petsAndVisitsTitle) {
                this.petsAndVisitsTitle.style.display = 'none'
            }
        })
    }

    componentWillMount() {
        this.setOwner()
    }

    componentDidUpdate() {
        if(this.state.pets.length === 0) {
            this.setPets()
        }
    }

    render() {
        return (
            <div className="container">
                <h2>Owner Information</h2>

                {!this.state.loadingOwner ?
                    <div>
                        {this.state.owner && 
                            <table>
                                <tbody>
                                    <OwnerInformationTableEntry title="Name" value={this.state.owner.firstname + ' ' + this.state.owner.lastname} />
                                    <OwnerInformationTableEntry title="Address" value={this.state.owner.address} />
                                    <OwnerInformationTableEntry title="City" value={this.state.owner.city} />
                                    <OwnerInformationTableEntry title="Telephone" value={this.state.owner.telephone} />
                                </tbody>
                            </table>
                        }

                        <div style={{marginTop: '10px'}}>
                            <button onClick={(e) => this.props.history.push('/ownerEdit/' + this.state.owner.id)}>Edit Owner</button>{' '}
                            <button onClick={(e) => this.props.history.push('/petCreation/' + this.state.owner.id)}>Add New Pet</button>
                        </div>

                        <h2 ref={petsAndVisitsTitle => this.petsAndVisitsTitle = petsAndVisitsTitle}>Pets &amp; visits</h2>
                        {!this.state.loadingPets ?
                            <table>
                                <tbody>
                                    {this.state.pets}
                                </tbody>
                            </table> : <Loader />
                        }
                    </div> : 
                    <Loader />
                }
            </div>
        )
    }

}