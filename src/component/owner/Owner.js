import React from 'react'
import axios from 'axios'
import Loader from '../common/Loader'
import { withRouter } from 'react-router-dom'

class Owner extends React.Component {
    state = {
        pets: null
    }

    getPets = async () => {
        const response = await axios.get('http://localhost:8080/v1/getPets/' + this.props.id)
        return await response
    }

    setPets = () => {
        let pets = this.state.pets

        this.getPets().then((response) => {
            response.data.forEach(pet => {
                if(!pets) {
                    pets = pet.name
                }else {
                    pets += ', ' + pet.name
                }
            })

            this.setState({pets})
        }).catch(err => this.setState({pets: 'None'}))
    }

    componentWillMount = () => {
      this.setPets()
    }

    render() {
        const {firstname, lastname, address, city, telephone} = this.props

        return (
            <tr style={{cursor: 'pointer'}} onClick={(e) => this.props.history.push('/ownerInformation/' + lastname)}>
                <td>{firstname + ' ' + lastname}</td>
                <td>{address}</td>
                <td>{city}</td>
                <td>{telephone}</td>
                <td>{(this.state.pets) ? this.state.pets : <Loader height={16} width={16} />}</td>
            </tr>
        )
    }
}

export default withRouter(Owner)