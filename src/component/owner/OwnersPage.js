import React, { Component } from 'react'
import axios from 'axios'
import '../../assets/css/OwnersPage.css'
import Loader from '../common/Loader'
import Owner from './Owner'
export default class OwnersPage extends Component {

    state = {
        owners: [],
        loading: true
    }

    getOwners = async () => {
        const response = await axios.get('http://localhost:8080/v1/getOwners')
        return await response
    }

    setOwners = () => {
        this.getOwners()
        .then((response) => {
            let owners = []

            response.data.forEach(owner => {
                owners.push(
                    <Owner key={owner.lastname} {...owner} />
                )
            })

            this.setState({owners, loading: false})
        })
        .catch(err => this.props.history.push('/error'))
    }

    componentWillMount() {
        this.setOwners()
    }
    
    render() {
        return (
            <div className="container">
                <h2>Owners</h2>

                {!this.state.loading ?
                    <table className="vets">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Telephone</th>
                                <th>Pets</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.owners}
                        </tbody>
                    </table> : <Loader />
                }
            </div>
        )
    }
    
}
