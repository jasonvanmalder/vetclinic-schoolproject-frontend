import React, { Component } from 'react'
import '../../assets/css/OwnersPage.css'
import axios from 'axios'
import Loader from '../common/Loader'
import FormChecker from '../../services/FormChecker'

export default class OwnerEdit extends Component {

    state = {
        owner: null,
        loadingOwner: true
    }

    getOwner = async () => {
        const response = await axios.get('http://localhost:8080/v1/getOwnerById/' + this.props.match.params.id);
        return await response
    }

    setOwner = () => {
        this.getOwner()
        .then(response => this.setState({owner: response.data, loadingOwner: false}))
        .catch(err => this.props.history.push('/error'))
    }

    editOwner = async () => {
        const Owner = {
            id: this.props.match.params.id,
            firstname: this.firstnameInput.value,
            lastname: this.lastnameInput.value,
            address: this.addressInput.value,
            city: this.cityInput.value,
            telephone: this.telephoneInput.value,
        }

        if(FormChecker.isString(Owner.firstname, 2)) {
            if(FormChecker.isString(Owner.lastname, 2)) {
                if(FormChecker.checkLength(Owner.address, 10)) {
                    if(FormChecker.isString(Owner.city, 4)) {
                        if(FormChecker.isPhoneNumber(Owner.telephone)) {
                            try {
                                await axios.put('http://localhost:8080/v1/editOwner/' + this.props.match.params.id, Owner);
                                this.props.history.push('/ownerInformation/' + this.state.owner.lastname);     
                            } catch (e) {
                                this.infoParagraph.innerHTML = 'Can\'t Edit owner'
                            }
                    
                            if (this.infoParagraph) {
                                this.infoParagraph.style.display = 'block'
                            }
                        }else {
                            FormChecker.errorMessage('Invalid phone number')
                        }
                    }else {
                        FormChecker.errorMessage('Invalid city')
                    }
                }else {
                    FormChecker.errorMessage('Invalid address')
                }
            }else {
                FormChecker.errorMessage('Invalid name')
            }
        }else {
            FormChecker.errorMessage('Invalid firstname')
        }
    }

    componentWillMount() {
        this.setOwner();
    }

    render() {
        return (
            <div className="container">
                <h2>Owner Edit</h2>

                <p ref={(info) => this.infoParagraph = info} style={{display:'none'}}></p>

                {!this.state.loadingOwner ?
                    <div>
                        {this.state.owner &&
                            <div className="form-container">
                                <div className="input-group">
                                    <label>First name</label>
                                    <input type="text" placeholder="First name" defaultValue={this.state.owner.firstname} ref={(input) => this.firstnameInput = input} />
                                </div>

                                <div className="input-group">
                                    <label>Last name</label>
                                    <input type="text" placeholder="Last name" defaultValue={this.state.owner.lastname} ref={(input) => this.lastnameInput = input} />
                                </div>

                                <div className="input-group">
                                    <label>Address</label>
                                    <input type="text" placeholder="Address" defaultValue={this.state.owner.address} ref={(input) => this.addressInput = input} />
                                </div>

                                <div className="input-group">
                                    <label>City</label>
                                    <input type="text" placeholder="City" defaultValue={this.state.owner.city} ref={(input) => this.cityInput = input} />
                                </div>

                                <div className="input-group">
                                    <label>Telephone</label>
                                    <input type="text" placeholder="Telephone" defaultValue={this.state.owner.telephone} ref={(input) => this.telephoneInput = input} />
                                </div>

                                <button onClick={this.editOwner}>Edit Owner</button>
                            </div>
                        }
                    </div> : <Loader />
                }
            </div>
        )
    }
    
}
