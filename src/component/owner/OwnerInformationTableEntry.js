import React from 'react'

const OwnerInformationTableEntry = ({title, value}) => {
    return (
        <tr>
            <td>{title}</td>
            <td>{value}</td>
        </tr>
    )
}

export default OwnerInformationTableEntry