import React, { Component } from 'react'
import '../../assets/css/OwnersPage.css'
import FormChecker from '../../services/FormChecker'

export default class FindOwner extends Component {

    findOwner = () => {
        const lastname = this.lastnameInput.value
        if(lastname) {
            if(FormChecker.isString(lastname, 3)) {
                this.props.history.push('/ownerInformation/' + lastname)
            }else {
                FormChecker.errorMessage('Invalid name')
            }
        }
        else this.props.history.push('/owners')
    }

    render() {
        return (
            <div className="container">
                <h2>Find Owners</h2>

                <div className="error" style={{display: 'none'}} ref={errorContainer => this.errorContainer = errorContainer}>
                    <p ref={errorMessage => this.errorMessage = errorMessage}></p>
                </div>
                <div className="form-container">
                    <div className="input-group">
                        <label>Last name</label>
                        <input type="text" placeholder="Last name" onKeyPress={(e) => { if(e.key === 'Enter'){this.findOwner()} }} ref={(input) => this.lastnameInput = input} />
                    </div>
                    <button onClick={this.findOwner}>Find Owner</button>
                </div>
                
                <button onClick={(e) => this.props.history.push('/ownerCreation')}>Add Owner</button>
            </div>
        )
    }
    
}
