import React, { Component } from 'react'
import '../../assets/css/OwnersPage.css'
import axios from 'axios'
import Loader from '../common/Loader'
import FormChecker from '../../services/FormChecker'

export default class VisitCreation extends Component {

    state = {
        pet: null,
        owner: null,
        visits: [],
        loadingPet: true
    }

    getPet = async () => {
        const response = await axios.get('http://localhost:8080/v1/getPet/' + this.props.match.params.pet_id)
        return await response
    }

    setPet = () => {
        this.getPet()
        .then(response => this.setState({pet: response.data, loadingPet: false}))
    }

    getOwner = async () => {
        const response = await axios.get('http://localhost:8080/v1/getOwnerById/' + this.state.pet.ownerId);
        return await response
    }

    setOwner = () => {
        this.getOwner()
        .then(response => this.setState({owner: response.data}))
        .catch(err => this.props.history.push('/error'))
    }

    getVisits = async () => {
        const response = await axios.get('http://localhost:8080/v1/getVisits/' + this.state.pet.id)
        return await response
    }

    setVisits = () => {
        this.getVisits()
        .then((response) => {
            let visits = []

            response.data.forEach(visit => {
                visits.push(
                    <tr key={visit.id}>
                        <td>{visit.date}</td>
                        <td>{visit.description}</td>
                    </tr> 
                )
            });

            this.setState({visits});
        })
        .catch(err => {
            this.previousTitle.style.display = 'none'
            this.previousTable.style.display = 'none'
        })
    }

    createVisit = async () => {
        const Visit = {
            id: 0,
            petId: parseInt(this.props.match.params.pet_id),
            date: this.dateInput.value,
            description: this.descriptionInput.value,
        }

        if(FormChecker.isDate(Visit.date)) {
            if(FormChecker.checkLength(Visit.description, 5)) {
                try {
                    await axios.post('http://localhost:8080/v1/createVisit', Visit);
                    this.props.history.push('/ownerInformation/' + this.state.owner.lastname);
                } catch (e) {
                    if (this.infoParagraph) {
                        this.infoParagraph.innerHTML = 'Can\'t add visit'
                        this.infoParagraph.style.display = 'block'
                    }
                }
                 
            }else {
                FormChecker.errorMessage('Invalid description')
            }
        }else {
            FormChecker.errorMessage('Invalid date')
        }
    }
    
    componentWillMount() {
        this.setPet()
    }

    componentDidUpdate() {
        if(!this.state.owner) {
            this.setOwner()
        }

        if(this.state.visits.length === 0) {
            this.setVisits()
        }
    }

    render() {
        return (
            <div className="container">
                <h2>New Visit</h2>

                <p ref={(info) => this.infoParagraph = info} style={{display:'none'}}></p>

                {!this.state.loadingPet ?
                    <div>
                        <h5>Pet</h5>
                        {(this.state.pet && this.state.owner) && 
                            <table className="vets">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Birth Date</th>
                                        <th>Type</th>
                                        <th>Owner</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            {this.state.pet.name}
                                        </td>
                                        <td>
                                            {this.state.pet.birthdate}
                                        </td>
                                        <td>
                                            {this.state.pet.type}
                                        </td>
                                        <td>
                                            {this.state.owner.firstname + ' ' + this.state.owner.lastname}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        }

                        <div className="form-container">
                            <div className="input-group">
                                <label>Date</label>
                                <input type="text" placeholder="DD-MM-YYYY" ref={(input) => this.dateInput = input} />
                            </div>

                            <div className="input-group">
                                <label>Description</label>
                                <input type="text" placeholder="Description" ref={(input) => this.descriptionInput = input} />
                            </div>

                            <button onClick={this.createVisit}>Add Visit</button>
                        </div>

                        <h5 ref={previousTitle => this.previousTitle = previousTitle}>Previous visits</h5>
                        <table ref={previousTable => this.previousTable = previousTable}>
                            <tbody>
                                <tr style={{borderBottom: '1px solid #000'}}>
                                    <td>Date</td>
                                    <td>Description</td>
                                </tr>
                                {this.state.visits}
                            </tbody>
                        </table>
                    </div> : <Loader />
                }
            </div>
        )
    }

}
