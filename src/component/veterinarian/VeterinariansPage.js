import React, { Component } from 'react'
import axios from 'axios'
import '../../assets/css/VeterinariansPage.css'
import Loader from '../common/Loader'
import Veterinarian from './Veterinarian';

export default class VeterinariansPage extends Component {

    state = {
        vets: [],
        loading: true
    }

    getVets = async () => {
        const response = await axios.get('http://localhost:8080/v1/getVets')
        return await response
    }

    setVets = () => {
        this.getVets()
        .then((response) => {
            let vets = []

            response.data.forEach(vet => {
                vets.push(
                    <Veterinarian key={vet.lastname} {...vet} />
                )
            })

            this.setState({vets, loading: false})
        })
        .catch(err => this.props.history.push('/error'))
    }

    componentWillMount() {
        this.setVets()
    }
    
    render() {
        return (
            <div className="container">
                <h2>Veterinarians</h2>

                {!this.state.loading ?
                    <table className="vets">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Specialities</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.vets}
                        </tbody>
                    </table> : <Loader />
                }
            </div>
        )
    }
    
}
