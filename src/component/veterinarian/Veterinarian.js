import React from 'react'

const Veterinarian = ({firstname, lastname, specialities}) => {
    return (
        <tr>
            <td>{ firstname + ' ' + lastname }</td>
            <td>{ specialities }</td>
        </tr>
    )
}

export default Veterinarian;