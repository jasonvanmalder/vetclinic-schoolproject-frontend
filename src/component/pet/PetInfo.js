import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

export default class PetInfo extends Component {
    
    state = {
        visits: [],
    }

    getVisits = async () => {
        const response = await axios.get('http://localhost:8080/v1/getVisits/' + this.props.id)
        return await response
    }

    setVisits = () => {
        this.getVisits()
        .then((response) => {
            let visits = []

            response.data.forEach(visit => {
                visits.push(
                    <tr key={visit.id}>
                        <td>{visit.date}</td>
                        <td>{visit.description}</td>
                    </tr> 
                )
            });

            this.setState({visits})
        })
    }

    componentDidMount() {
        this.setVisits()
    }

    render() {
        return (
            <tr>
                <td>
                    <dl>
                        <dt>Name</dt>
                        <dd>{this.props.name}</dd>

                        <dt>Birth Date</dt>
                        <dd>{this.props.birthdate}</dd>

                        <dt>Type</dt>
                        <dd>{this.props.type}</dd>
                    </dl>
                </td>
                <td>
                    <table className="no-border">
                        <thead>
                            <tr>
                                <th>Visit Date</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.visits}
                            <tr>
                                <td style={{backgroundColor: 'initial'}}>
                                    <Link to={'/editPet/' + this.props.id}>Edit Pet</Link>
                                </td>
                                <td style={{backgroundColor: 'initial'}}>
                                    <Link to={'/visitCreation/' + this.props.id}>Add Visit</Link>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        )
    }

}
