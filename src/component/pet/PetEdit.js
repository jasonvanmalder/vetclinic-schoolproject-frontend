import React, { Component } from 'react'
import '../../assets/css/OwnersPage.css'
import axios from 'axios'
import Loader from '../common/Loader'
import FormChecker from '../../services/FormChecker'

export default class PetEdit extends Component {

    state = {
        pet: null,
        owner: null,
        loadingPet: true
    }

    getPet = async () => {
        const response = await axios.get('http://localhost:8080/v1/getPet/' + this.props.match.params.id)
        return await response
    }

    setPet = () => {
        this.getPet()
        .then(response => this.setState({pet: response.data, loadingPet: false}))
        .catch(err => this.props.history.push('/error'))
    }

    getOwner = async () => {
        const response = await axios.get('http://localhost:8080/v1/getOwnerById/' + this.state.pet.ownerId);
        return await response
    }

    setOwner = () => {
        this.getOwner()
        .then(response => this.setState({owner: response.data}))
        .catch(err => this.props.history.push('/error'))
    }

    updatePet = async () => {
        let pets = ['Bird', 'Cat','Dog', 'Hamster', 'Lizard', 'Snake']

        const Pet = {
            id: this.state.pet.id,
            ownerId: this.state.pet.ownerId,
            name: this.nameInput.value,
            birthdate: this.birthdateInput.value,
            type: this.typeInput.value,
        }

        if(FormChecker.isString(Pet.name, 2)) {
            if(FormChecker.isDate(Pet.birthdate)) {
                if(pets.includes(Pet.type)) {
                    try {
                        await axios.put('http://localhost:8080/v1/editPet/' + this.state.pet.id, Pet);
                        this.props.history.push('/ownerInformation/' + this.state.owner.lastname);
                    } catch(e) {
                        if (this.infoParagraph) {
                            this.infoParagraph.innerHTML = 'Can\'t edit pet'
                            this.infoParagraph.style.display = 'block'
                        }
                    }
                }else {
                    FormChecker.errorMessage('Invalid type')
                }
            }else {
                FormChecker.errorMessage('Invalid birthdate')
            }
        }else {
            FormChecker.errorMessage('Invalid name')
        }
    }

    componentWillMount() {
        this.setPet()
    }

    componentDidUpdate() {
        if(!this.state.owner) {
            this.setOwner()
        }
        
        if(this.state.pet) {
            switch(this.state.pet.type) {
                case 'Cat': this.typeInput.selectedIndex = 1; break;
                case 'Dog': this.typeInput.selectedIndex = 2; break;
                case 'Hamster': this.typeInput.selectedIndex = 3; break;
                case 'Lizard': this.typeInput.selectedIndex = 4; break;
                case 'Snake': this.typeInput.selectedIndex = 5; break;
                default: this.typeInput.selectedIndex = 0; break;
            }
        }
    }

    render() {
        return (
            <div className="container">
                {!this.state.loadingPet ?
                    <div>
                        <h2>Pet</h2>

                        <p ref={(info) => this.infoParagraph = info} style={{display:'none'}}></p>

                        {this.state.pet &&
                            <div className="form-container">
                                <div className="input-group">
                                    <label>Owner</label>
                                    {this.state.owner && 
                                        <input type="text" ref={(input) => this.ownerInput = input} defaultValue={this.state.owner.firstname + ' ' + this.state.owner.lastname} disabled />
                                    }
                                </div>

                                <div className="input-group">
                                    <label>Name</label>
                                    <input type="text" placeholder="Name" defaultValue={this.state.pet.name} ref={(input) => this.nameInput = input} />
                                </div>

                                <div className="input-group">
                                    <label>Birthdate</label>
                                    <input type="text" placeholder="DD-MM-YYYY" defaultValue={this.state.pet.birthdate} ref={(input) => this.birthdateInput = input} />
                                </div>

                                <div className="input-group">
                                    <label>Type</label>
                                    <select ref={(input) => this.typeInput = input} >
                                        <option>Bird</option>
                                        <option>Cat</option>
                                        <option>Dog</option>
                                        <option>Hamster</option>
                                        <option>Lizard</option>
                                        <option>Snake</option>
                                    </select>
                                </div>

                                <button onClick={this.updatePet}>Edit Pet</button>
                            </div>
                        }
                    </div> : <Loader />
                }
            </div>
        )
    }

}
