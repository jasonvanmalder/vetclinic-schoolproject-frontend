import React, { Component } from 'react'
import '../../assets/css/OwnersPage.css'
import axios from 'axios'
import Loader from '../common/Loader'
import FormChecker from '../../services/FormChecker'

export default class PetCreation extends Component {

    state = {
        owner: null,
        loadingPet: true
    }

    getOwner = async () => {
        const response = await axios.get('http://localhost:8080/v1/getOwnerById/' + this.props.match.params.owner_id);
        return await response
    }

    setOwner = () => {
        this.getOwner()
        .then(response => this.setState({owner: response.data, loadingPet: false}))
        .catch(err => this.props.history.push('/error'))
    }

    createPet = async () => {
        let pets = ['Bird', 'Cat','Dog', 'Hamster', 'Lizard', 'Snake']
        
        const Pet = {
            id: 0,
            ownerId: parseInt(this.props.match.params.owner_id),
            name: this.nameInput.value,
            birthdate: this.birthdateInput.value,
            type: this.typeInput.value,
        }

        if(FormChecker.isString(Pet.name, 2)) {
            if(FormChecker.isDate(Pet.birthdate)) {
                if(pets.includes(Pet.type)) {
                    try {
                        await axios.post('http://localhost:8080/v1/createPet', Pet)
                        this.props.history.push('/ownerInformation/' + this.state.owner.lastname);
                    } catch (e) {
                        if (this.infoParagraph) {
                            this.infoParagraph.innerHTML = 'Can\'t add pet'
                            this.infoParagraph.style.display = 'block'
                        }
                    }
                    
                }else {
                    FormChecker.errorMessage('Invalid type')
                }
            }else {
                FormChecker.errorMessage('Invalid birthdate')
            }
        }else {
            FormChecker.errorMessage('Invalid name')
        }
    }

    componentWillMount() {
        this.setOwner();
    }

    render() {
        return (
            <div className="container">
                <h2>New Pet</h2>

                <p ref={(info) => this.infoParagraph = info} style={{display:'none'}}></p>

                {!this.state.loadingPet ?
                    <div className="form-container">
                        <div className="input-group">
                            <label>Owner</label>
                            {this.state.owner && 
                                <input type="text" ref={(input) => this.ownerInput = input} value={this.state.owner.firstname + ' ' + this.state.owner.lastname} disabled />
                            }
                        </div>

                        <div className="input-group">
                            <label>Name</label>
                            <input type="text" placeholder="Name" ref={(input) => this.nameInput = input} />
                        </div>

                        <div className="input-group">
                            <label>Birthdate</label>
                            <input type="text" placeholder="DD-MM-YYYY" ref={(input) => this.birthdateInput = input} />
                        </div>

                        <div className="input-group">
                            <label>Type</label>
                            <select ref={(input) => this.typeInput = input}>
                                <option>Bird</option>
                                <option>Cat</option>
                                <option>Dog</option>
                                <option>Hamster</option>
                                <option>Lizard</option>
                                <option>Snake</option>
                            </select>
                        </div>

                        <button onClick={this.createPet}>Add Pet</button>
                    </div> : <Loader />
                }
            </div>
        )
    }

}
